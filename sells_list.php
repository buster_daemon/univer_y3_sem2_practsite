<html>
    <head>
        <title>LinTech</title>
        <meta name="viewport" content="width=device-width, initial-scale='1.0'" charset="utf-8">
        <link href="style.css" rel="stylesheet">
    </head>
    <body>
		<div class="root">
			<div class="top-bar">
            <?php				
				include("navbar.php");
				?>
			</div>
			<div class="main">
				<h1>LinTech</h1>
				<h2>Магазин Linux-совместимого оборудования</h2>
				<p>Список товаров</p>
				<br>
                <?php 
                    $handle = new mysqli('localhost', 'root', 'test', 'lintech');
                    $result = $handle->query("SELECT id, qty, cost, nametov FROM sells");
                    if (!$result) {
                        echo mysqli_error($handle);
                        echo "<p>Ошибка обработки запроса</p>
                        <p><a href='sells_list.php'>Перезагрузить страницу</a></p>";                        
                        exit;
                    }
                    else {
                        $numres = $result->num_rows;                        
                        echo "<table border='1'>";
                        echo "<th>Номер товара</th>
                        <th>Количество на складе (шт.)</th>
                        <th>Стоимость товара (за шт.)</th>
                        <th>Наименование товара</th>
                        <th>Действие</th>";
                        for ($i = 0; $i < $numres; $i++) {
                            $row = $result->fetch_assoc();
                            echo "<tr>";
                            echo "<form action='sl_conf.php' method='post'>";
                            echo "<td><input disabled type='number' value=" . $row['id'] . "></td>";
                            echo "<td><input type='number' name='qty' value=" . $row['qty'] . "></td>";
                            echo "<td><input type='number' name='cost' value=" . $row['cost'] . "></td>";
                            echo '<td><input disabled type="text" size="25" value="' . $row['nametov'] . '"></td>';                            
                            echo "<td><input type='submit' value='Изменить'>
                            <input type='hidden' name='ids' value=" . $row['id'] . "></td>";
                            echo "</form>";
                            echo "</tr>";
                        }
                        echo "</table>";                                                
                    }
                ?>
            </div>
            <?php
			include("footer.php");
			?>
        </div>
    </body>
</html>