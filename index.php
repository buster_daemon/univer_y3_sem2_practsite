<html>
    <head>
        <title>LinTech</title>
        <meta name="viewport" content="width=device-width, initial-scale='1.0'" charset="utf-8">
        <link href="style.css" rel="stylesheet">
    </head>
    <body>
		<div class="root">
			<div class="top-bar">
				<?php				
				include("navbar.php");
				?>
			</div>
			<div class="main">
				<h1>LinTech</h1>
				<h2>Магазин Linux-совместимого оборудования</h2>
				<p>Последние поставки</p>
				<br>
				<?php
				$handle = new mysqli('localhost', 'root', 'test', 'lintech');
				$query = 'SELECT id, qty, cost, nametov FROM sells';
				$result = $handle->query($query);
				$numres = $result->num_rows;
				if (!$result) {
					echo "NotLikeThis";
					exit;
				}
				else {
					echo "<table border='0'>
					<tr><td>";
					for ($i = 0; $i < $numres; $i++) {
						$row = $result->fetch_assoc();
						echo "<div class='tov'>";

						if ($row['id'] == 1) {
							echo "<img src='img/rpi3b.jpg'>";
						}
						else if ($row['id'] == 2) {
							echo "<img src='img/rpi4.jpg'>";
						}
						else if ($row['id'] == 3) {
							echo "<img src='img/ardpro3.jpg'>";
						}
						else if ($row['id'] == 4) {
							echo "<img src='img/libr5.png'>";
						}
						else if ($row['id'] == 5) {
							echo "<img src='img/mintbox3.jpg'>";
						}
						else if ($row['id'] == 6) {
							echo "<img src='img/netgate1100.jpg'>";
						}
						else if ($row['id'] == 7) {
							echo "<img src='img/pineppro.png'>";
						}

						echo "<br>";
						echo "<p>" . $row['nametov'] . "</p>";
						echo "<p>Стоимость: " . $row['cost'] . " рублей</p>";						
						if ($row['qty'] != 0) {
							echo "<p>Количество: " . $row['qty'] . " шт.</p>";
							echo "<form action='order.php' method='post'>";
							echo "<input type='submit' value='Купить'>";
							echo "<input type='hidden' name='idzakaz' value=" . $row['id'] . ">";
							echo "</form>";
						}
						else {
							echo "<br>Нет в продаже";
						}
						echo "</div>";
					}
					echo "</td></tr></table>";
				}
				?>
			</div>
			<?php
			include("footer.php");
			?>
		</div>
    </body>
</html>