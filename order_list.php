<html>
    <head>
        <title>LinTech</title>
        <meta name="viewport" content="width=device-width, initial-scale='1.0'" charset="utf-8">
        <link href="style.css" rel="stylesheet">
    </head>
    <body>
		<div class="root">
			<div class="top-bar">
            <?php				
				include("navbar.php");
				?>
			</div>
			<div class="main">
				<h1>LinTech</h1>
				<h2>Магазин Linux-совместимого оборудования</h2>
				<p>Оформленные заказы</p>
				<br>
				<?php
				$handle = new mysqli('localhost', 'root', 'test', 'lintech');
				$query_main = 'SELECT id, id_sell, name, sname, addresso, dateord, qtyo, summo FROM orders';
                $query_sells = 'SELECT id, nametov FROM sells';
				$result = $handle->query($query_main);
				$numres = $result->num_rows;
                echo mysqli_error($handle);
				if (!$result) {
					echo "NotLikeThis";
					exit;
				}
				else {
                    echo "<hr>";
                    echo "Количество заказов: " . $numres;  
                    echo "<div class='orls'>";                  
                    echo "<table border='1'>";  
                    echo "<th>Номер заказа</th>";
                    echo "<th>Номер товара</th>";
                    echo "<th>Имя покупателя</th>";
                    echo "<th>Фамилия покупателя</th>";
                    echo "<th>Адрес покупателя</th>";
                    echo "<th>Дата заказа</th>";
                    echo "<th>Количество товара</th>";
                    echo "<th>Сумма заказа (руб.)</th>";
                    echo "<th>Действия</th>";
                    for($i = 0; $i < $numres; $i++) {
                        $row = $result->fetch_assoc();
                        echo "<tr>";
                        echo "<td>" . $row['id'] . "</td>";
                        echo "<td>" . $row['id_sell'] . "</td>";
                        echo "<td>" . $row['name'] . "</td>";
                        echo "<td>" . $row['sname'] . "</td>";
                        echo "<td>" . $row['addresso'] . "</td>";
                        echo "<td>" . $row['dateord'] . "</td>";
                        echo "<td>" . $row['qtyo'] . "</td>";
                        echo "<td>" . $row['summo'] . "</td>";
                        echo "<td><form action='edit_ord.php' method='post'><input type='submit' value='Редактировать заказ'>
                        <input type='hidden' name='idze' value=".$row['id']."></form>";
                        echo "<form action='del_ord.php' method='post'><input type='submit' value='Удалить заказ'>
                        <input type='hidden' name='idzd' value=".$row['id']."></form>";
                        echo "</td>";
                        echo "</tr>";
                    }
                    echo "</table>";
                    echo "</div>";
                    echo "<hr>";
                    $result = $handle->query($query_sells);
                    $numres = $result->num_rows;
                    echo "Количество товаров: " . $numres;
                    echo "<div class='orls'>";
                    echo "<table border='1'>";
                    echo "<th>Номер товара</th>";
                    echo "<th>Название товара</th>";
                    for ($i = 0; $i < $numres; $i++) {
                        $row = $result->fetch_assoc();
                        echo "<tr>";
                        echo "<td>" . $row['id'] . "</td>";
                        echo "<td>" . $row['nametov'] . "</td>";
                        echo "</tr>";
                    }
                    echo "</table>";
                    echo "</div>";
                }

                ?>
            </div>
            <?php
			include("footer.php");
			?>
        </div>
    </body>
</html>